const l = require('node-lifx');
const client = new l.Client();
const _ = require('lodash');

const bright = 40;
var updateToOpen = function(light){
	console.log('updating to open', light);
	light.on();
	light.color(120, 80, bright, 3500, 2000);
	light.color(120, 80, bright, 3500, 2000);
	setTimeout(() => { 
		light.color(120, 80, bright, 3500, 2000);
	}, 50);
};

var updateToWarning = function(light){
	light.on();
	light.color(90, 80, 80, 3500, 2000);
};

var updateToBusy = function(light){
	light.on();
	light.color(350, 80, bright, 3500, 2000);
	light.color(350, 80, bright, 3500, 2000);
	
	setTimeout(() => { 
	light.color(350, 80, bright, 3500, 2000);
	}, 50);
};

var restartDiscovery = function(){
	console.log('restarting discovery');
	let keysToDelete = [];
	for (var key in client.devices){
		if(client.devices[key].label === null){
			keysToDelete.push(key);
		}
	}

	keysToDelete.forEach(k => { delete client.devices[key] });
	console.log('deleted keys ', JSON.stringify(keysToDelete, null, 2));
	console.log(client.devices);
	client.startDiscovery();
};

client.on('new-light', function(light){
	console.log('found a new light', light);
	mylights[light.label] = light;
	if(Object.keys(mylights).length === 2){
		client.stopDiscovery();
		console.log('stopping discovery, found all the bulbs');
	}
});
console.log('init the client');

client.init({
	resendPacketDelay : 250,
	resendMaxTimes: 5
});





var updateLights = function(){
	console.log('in the update lights function');
	console.log(client.devices);
	let needToSearchAgain = false;
	for (var key in client.devices){
		console.log(key, 'key');
		
		const currentDevice = client.devices[key];
		console.log(currentDevice, 'currentDevice');
		const label = client.devices[key].label;
		needToSearchAgain = label === null ||  needToSearchAgain;
		const lightStatusBool = latestData[label];
		const lightStatus = lightStatusBool ? 'busy' : 'open';
		console.log(lightStatus, ' lightStatus');
		switch(lightStatus) {
			case 'open':
				updateToOpen(client.devices[key]);
				break;
			case 'warning':
				updateToWarning(client.devices[key]);
				break;
			case 'busy':
				updateToBusy(client.devices[key]);
				break;
			default:
				break;
		}
	}
	if(needToSearchAgain){
		console.log('restarting discovery, some label was null');
		restartDiscovery();
	}
};






console.log('setting up firebase');

const Firebase = require('firebase');
const ref = new Firebase('https://radiant-heat-6040.firebaseio.com/');
let latestData;

ref.child('active').on('value', function(snapshot){
	latestData = snapshot.val();
	console.log(snapshot.val());
	updateLights();
});

updateLights();
