'use strict';

var l = require('node-lifx');
var client = new l.Client();
var _ = require('lodash');

var bright = 40;
var updateToOpen = function updateToOpen(light) {
	console.log('updating to open', light);
	light.on();
	light.color(120, 80, bright, 3500, 2000);
	light.color(120, 80, bright, 3500, 2000);
	setTimeout(function () {
		light.color(120, 80, bright, 3500, 2000);
	}, 50);
};

var updateToWarning = function updateToWarning(light) {
	light.on();
	light.color(90, 80, 80, 3500, 2000);
};

var updateToBusy = function updateToBusy(light) {
	light.on();
	light.color(350, 80, bright, 3500, 2000);
	light.color(350, 80, bright, 3500, 2000);

	setTimeout(function () {
		light.color(350, 80, bright, 3500, 2000);
	}, 50);
};

var restartDiscovery = function restartDiscovery() {
	console.log('restarting discovery');
	var keysToDelete = [];
	for (var key in client.devices) {
		if (client.devices[key].label === null) {
			keysToDelete.push(key);
		}
	}

	keysToDelete.forEach(function (k) {
		delete client.devices[key];
	});
	console.log('deleted keys ', JSON.stringify(keysToDelete, null, 2));
	console.log(client.devices);
	client.startDiscovery();
};

client.on('new-light', function (light) {
	console.log('found a new light', light);
	mylights[light.label] = light;
	if (Object.keys(mylights).length === 2) {
		client.stopDiscovery();
		console.log('stopping discovery, found all the bulbs');
	}
});
console.log('init the client');

client.init({
	resendPacketDelay: 250,
	resendMaxTimes: 5
});

var updateLights = function updateLights() {
	console.log('in the update lights function');
	console.log(client.devices);
	var needToSearchAgain = false;
	for (var key in client.devices) {
		console.log(key, 'key');

		var currentDevice = client.devices[key];
		console.log(currentDevice, 'currentDevice');
		var label = client.devices[key].label;
		needToSearchAgain = label === null || needToSearchAgain;
		var lightStatusBool = latestData[label];
		var lightStatus = lightStatusBool ? 'busy' : 'open';
		console.log(lightStatus, ' lightStatus');
		switch (lightStatus) {
			case 'open':
				updateToOpen(client.devices[key]);
				break;
			case 'warning':
				updateToWarning(client.devices[key]);
				break;
			case 'busy':
				updateToBusy(client.devices[key]);
				break;
			default:
				break;
		}
	}
	if (needToSearchAgain) {
		console.log('restarting discovery, some label was null');
		restartDiscovery();
	}
};

console.log('setting up firebase');

var Firebase = require('firebase');
var ref = new Firebase('https://radiant-heat-6040.firebaseio.com/');
var latestData = void 0;

ref.child('active').on('value', function (snapshot) {
	latestData = snapshot.val();
	console.log(snapshot.val());
	updateLights();
});

updateLights();
